#!/usr/bin/env python

import sys
import rospy
import os
# import rosbag

from PyQt5.QtWidgets import *
from PyQt5.QtCore import QVariant
from PyQt5.QtWidgets import QListWidget
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import *
from PyQt5.QtGui import *

import subprocess
import signal
import shlex


class Form(QWidget):
    def __init__(self):
        QWidget.__init__(self, flags=Qt.Widget)
        self.setWindowTitle("ROS Bag logger")

        layout = QBoxLayout(QBoxLayout.TopToBottom)
        self.viewer = QListWidget(self)
        layout.addWidget(self.viewer)

        topics = rospy.get_published_topics()

        for topic in topics:
            item = QListWidgetItem(topic[0])
            item.setFlags(item.flags() | Qt.ItemIsUserCheckable)
            item.setCheckState(Qt.Unchecked)
            self.viewer.addItem(item)

        self.save_button = QPushButton("Save")
        self.stop_button = QPushButton("Stop")
        self.reload_button = QPushButton("Reload")

        layout.addWidget(self.save_button)
        layout.addWidget(self.stop_button)
        layout.addWidget(self.reload_button)

        self.setLayout(layout)

        self.save_button.clicked.connect(self.save_callback)
        self.stop_button.clicked.connect(self.stop_callback)
        self.reload_button.clicked.connect(self.stop_callback)

    def terminate_ros_node(self, s):
        # Adapted from http://answers.ros.org/question/10714/start-and-stop-rosbag-within-a-python-script/
        list_cmd = subprocess.Popen("rosnode list", shell=True, stdout=subprocess.PIPE)
        list_output = list_cmd.stdout.read()
        retcode = list_cmd.wait()
        assert retcode == 0, "List command returned %d" % retcode
        for str in list_output.split("\n"):
            if (str.startswith(s)):
                os.system("rosnode kill " + str)

    def save_callback(self):
        print("save")
        command = "rosbag record"
        for n in range(self.viewer.count()):
            item = self.viewer.item(n)
            if (item.checkState() != 0):
                print(item.checkState())
                print(item.text())
                command = command + " " + item.text()
        self.rosbag_proc = subprocess.Popen(
            command, stdin=subprocess.PIPE, shell=True, cwd='/home/dogu/catkin_ws')

    def stop_callback(self):
        print("stop")
        self.terminate_ros_node("/record")

    def reload_callback(self):
        #TODO reload topic list
        print("reload")


if __name__ == "__main__":
    rospy.init_node('bag_logger', anonymous=True)
    app = QApplication(sys.argv)
    form = Form()
    form.show()
    exit(app.exec_())
