# bag_file_logger

## Usage

`$catkin_make --pkg bag_file_logger`

`$rosrun bag_file_logger ros_bag_logger.py`

## GUI Description

![image](./bag_file_logger.png)

* Check a topic needed to log
* Click save button, then `rosbag record` command execute
* Click stop button to kill record node
